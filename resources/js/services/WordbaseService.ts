import axios, {AxiosResponse} from "axios";
import {AuthService} from "./AuthService";

export class WordbaseService {

    public static async upload(form: FormData): Promise<number> {
        try {
            // We need to set headers for file upload
            const result: AxiosResponse<{ wordbase_id: number }> = await axios.post('/api/v1/wordbase', form, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
            if (result && result.status === 200) {
                return result.data.wordbase_id;
            }
        } catch (error) {
            // User is unauthorized and should be logged out
            if (error.response?.status === 401) {
                AuthService.logOut();
            }

            throw error;
        }

        return -1;
    }

    public static async search(wordbaseId: string, word: string): Promise<Array<string>> {
        try {
            const result: AxiosResponse<{ words: Array<string> }> = await axios.post('/api/v1/wordbase/' + wordbaseId + '/anagrams', {
                word: word,
            });
            if (result && result.status === 200) {
                return result.data.words;
            }
        } catch (error) {
            // User is unauthorized and should be logged out
            if (error.response?.status === 401) {
                AuthService.logOut();
            }

            throw error;
        }

        return [];
    }
}
