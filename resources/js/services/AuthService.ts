import axios, {AxiosResponse} from "axios";

export class AuthService {
    /**
     * Checks local storage if an item is set, when set user is assumed to be logged in.
     */
    public static isAuthenticated(): boolean {
        return !!window.localStorage.getItem('logged-in');
    }

    /**
     * Makes the log in ajax request.
     *
     * @param email
     * @param password
     */
    public static async login(email: string, password: string): Promise<boolean> {
        const result: AxiosResponse = await axios.post('/api/v1/user/login', {
            email: email,
            password: password,
        });
        if (result && result.status === 200) {
            // Set the local storage item so we know we are logged in
            window.localStorage.setItem('logged-in', '1');
            return true;
        } else {
            return false;
        }
    }

    /**
     * Log out function used when some request returns a 401 status code
     */
    public static logOut() {
        window.localStorage.removeItem('logged-in');
    }
}
