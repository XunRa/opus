import React from "react";
import ReactDOM from "react-dom";
import {Route, BrowserRouter, Switch} from "react-router-dom";
import Login from "./components/Login";
import Upload from "./components/Upload";
import Search from "./components/Search";
import ProtectedRoute from "./ProtectedRoute";

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Switch>
                <Route path="/login" component={Login}/>
                <ProtectedRoute exact={true} path="/upload" component={Upload}/>
                <ProtectedRoute path="/wordbase/:id" component={Search}/>
                <ProtectedRoute component={Upload}/>
            </Switch>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById("app")
);
