import React, {useState} from "react";
import {withRouter, RouteComponentProps, Redirect} from "react-router-dom";
import Loader from "./Loader";
import {WordbaseService} from "../services/WordbaseService";
import {AuthService} from "../services/AuthService";

const Upload = (props: RouteComponentProps) => {
    // Initialize states
    const [loading, setLoading] = useState({
        loading: false,
    });
    const [error, setError] = useState({
        error: false,
    });


    /**
     * File upload on button press
     *
     * @param e
     */
    const handleSubmitClick = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();

        // Put together FormData for ajax request to upload the file
        let formData = new FormData();
        const imageFile = document.querySelector('#file') as HTMLInputElement;
        // Basic check to make sure something was actually input
        if (!imageFile || !imageFile.files || imageFile.files.length <= 0) {
            setError({error: true});
            return;
        }
        formData.append("file", imageFile.files[0]);

        // Make the upload request
        setLoading({loading: true});
        WordbaseService.upload(formData).then((wordbaseId: number) => {
            setLoading({loading: false});
            if (wordbaseId > 0) {
                props.history.push('/wordbase/' + wordbaseId);
            } else {
                setError({error: true});
            }
        }).catch(() => {
            setLoading({loading: false});
            setError({error: true});
        })
    };

    if (loading.loading) {
        return <Loader/>;
    }
    if (!AuthService.isAuthenticated()) {
        return <Redirect to="/"/>;
    }

    return (
        <form>
            <h1 className="h3 mb-3 font-weight-normal">Please upload a wordbase</h1>
            {error.error &&
            <div className="alert alert-danger mb-4" role="alert">
                Something wen't wrong trying to upload your file. Please check your input.
            </div>
            }
            <input id="file" type="file" className="form-control"/>
            <button onClick={handleSubmitClick} className="btn btn-lg btn-primary btn-block" type="submit">
                Upload
            </button>
        </form>
    );
};

export default withRouter(Upload);
