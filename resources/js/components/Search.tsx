import React, {useState} from "react";
import {withRouter, Redirect, useParams} from "react-router-dom";
import Loader from "./Loader";
import {AuthService} from "../services/AuthService";
import {WordbaseService} from "../services/WordbaseService";

const Search = () => {
    // Initialize states
    const [state, setState] = useState({
        anagram: "",
    });
    const [loading, setLoading] = useState({
        loading: false,
    });
    const [words, setWords] = useState<{ words: Array<string> }>({
        words: [],
    });
    const [error, setError] = useState({
        error: false,
    });

    // Retrieve the wordbase id form uri
    const {id} = useParams();

    /**
     * Handles input element changes and state update
     *
     * @param e
     */
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {id, value} = e.target;
        setState((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    /**
     * Anagram search request
     *
     * @param e
     */
    const handleSubmitClick = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        // Make the request to find anagrams
        setLoading({loading: true});
        WordbaseService.search(id, state.anagram).then((words: Array<string>) => {
            setLoading({loading: false});
            if (words.length <= 0) {
                setWords({words: ["None found"]});
            } else {
                setWords({words: words});
            }
        }).catch(() => {
            setLoading({loading: false});
            setError({error: true});
        })
    };

    if (!AuthService.isAuthenticated()) {
        return <Redirect to="/"/>;
    }

    return (
        <form>
            <h1 className="h3 mb-3 font-weight-normal">Find an anagram</h1>
            {error.error &&
            <div className="alert alert-danger mb-4" role="alert">
                Something wen't wrong trying to find anagrams. Please check your input.
            </div>
            }
            <input type="text" id="anagram" className="form-control" placeholder="Word to find anagram to" value={state.anagram} onChange={handleChange}/>
            <button onClick={handleSubmitClick} className="btn btn-lg btn-primary btn-block" type="submit">
                Find
            </button>
            {!loading.loading && words.words.length > 0 &&
            <div>
                <h2 className="h3 mb-3 font-weight-normal">Results</h2>
                <List list={words.words}/>
            </div>
            }
            {loading.loading &&
            <Loader/>
            }
        </form>
    );
};

interface ListProps {
    list: Array<string>;
}

/**
 * Anagram list component
 *
 * @param props
 */
const List = (props: ListProps) => {
    return (
        <ul className="list-group">
            {props.list.map(item => (
                <li className="list-group-item">{item}</li>
            ))}
        </ul>
    );
}

export default withRouter(Search);
