import React from "react";

/**
 * Simple spinner to use when making ajax requests
 */
const Loader = () => {
    return (
        <div className="text-center mt-3">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
};

export default Loader;
