import React, {useState} from "react";
import {Redirect, RouteComponentProps, withRouter} from "react-router-dom";
import {AuthService} from "../services/AuthService";
import Loader from "./Loader";

/**
 * Display a login form
 *
 * @param props
 */
const Login = (props: RouteComponentProps) => {
    // Initialize states
    const [form, setForm] = useState({
        username: "",
        password: "",
    });
    const [error, setError] = useState({
        error: false,
    });
    const [loading, setLoading] = useState({
        loading: false,
    });

    /**
     * Handles input element changes and state update
     *
     * @param e
     */
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {id, value} = e.target;
        setForm((prevState) => ({
            ...prevState,
            [id]: value,
        }));
    };

    /**
     * User log in on submit button press
     *
     * @param e
     */
    const handleSubmitClick = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        setLoading({loading: true});

        AuthService.login(form.username, form.password).then((result: boolean) => {
            setLoading({loading: false});
            if (result) {
                setError({error: false});
                props.history.push("/");
            } else {
                setError({error: true});
            }
        }).catch(() => {
            setLoading({loading: false});
            setError({error: true});
        })
    };

    if (AuthService.isAuthenticated()) {
        return <Redirect to="/"/>;
    }
    if (loading.loading) {
        return <Loader/>;
    }

    return (
        <form>
            <img className="mb-4" src="https://www.opus.ee/resources/img/logo.svg" alt="" height="72"/>
            {error.error &&
            <div className="alert alert-danger mb-4" role="alert">
                Something wen't wrong trying to log you in. Please check your inputs.
            </div>
            }
            <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
            <label htmlFor="username" className="sr-only">
                Email address
            </label>
            <input type="text" id="username" className="form-control" placeholder="Username" value={form.username} onChange={handleChange}/>
            <label htmlFor="password" className="sr-only">
                Password
            </label>
            <input type="password" id="password" className="form-control" placeholder="Password" value={form.password} onChange={handleChange}/>
            <button onClick={handleSubmitClick} className="btn btn-lg btn-primary btn-block" type="submit">
                Sign in
            </button>
        </form>
    );
};

export default withRouter(Login);
