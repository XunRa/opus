import * as React from 'react';
import {Route, Redirect, RouteProps} from 'react-router-dom';
import {AuthService} from "./services/AuthService";

// Any is a bad type to use but react uses it itself on RouteProps
interface ProtectedRouteProps extends RouteProps {
    // tslint:disable-next-line:no-any
    component?: any;
    // tslint:disable-next-line:no-any
    children?: any;
}

/**
 * @author someone on stackoverflow but I lost the link
 * @param props
 */
const ProtectedRoute = (props: ProtectedRouteProps) => {
    const {component: Component, children, ...rest} = props;

    return (
        <Route
            {...rest}
            render={routeProps =>
                AuthService.isAuthenticated() ? (
                    Component ? (
                        <Component {...routeProps} />
                    ) : (
                        children
                    )
                ) : (
                    <Redirect to="/login"/>
                )
            }
        />
    );
};

export default ProtectedRoute;
