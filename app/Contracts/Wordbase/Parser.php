<?php

namespace App\Contracts\Wordbase;

interface Parser
{
    /**
     * Parse string to words
     *
     * @param string $contents
     *
     * @return string[]
     */
    public function parse(string $contents);

    /**
     * Sanitizes a single word
     *
     * @param string $word
     *
     * @return string
     */
    public function sanitizeSingle(string $word);
}
