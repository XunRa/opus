<?php

namespace App\Contracts\Wordbase;

interface Mapping
{
    /**
     * Generates a character mapping for the wordbase
     *
     * @param string[] $words
     *
     * @return array
     */
    public function generate(iterable $words);
}
