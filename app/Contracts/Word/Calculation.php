<?php

namespace App\Contracts\Word;

interface Calculation
{
    /**
     * @param array  $charMapping
     * @param string $word
     *
     * @return int
     */
    public function generate($charMapping = [], string $word = '');
}
