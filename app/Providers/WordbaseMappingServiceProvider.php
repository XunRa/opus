<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WordbaseMappingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Contracts\Wordbase\Mapping', 'App\Services\Wordbase\Mapping\Prime');
    }
}
