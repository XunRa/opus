<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WordbaseParserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\Wordbase\Parser',
            'App\Services\Wordbase\Parser\NewLineParser'
        );
    }
}
