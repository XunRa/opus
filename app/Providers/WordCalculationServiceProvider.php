<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WordCalculationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Contracts\Word\Calculation', 'App\Services\Word\Calculation\Prime');
    }
}
