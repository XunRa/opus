<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 *
 * @package App
 * @property int    calculated_value
 * @property string word
 * @property int    id
 */
class Word extends Model
{

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'word',
        'calculated_value',
        'wordbase_id'
    ];

    /**
     * Get the wordbase that owns this word
     */
    public function wordbase()
    {
        return $this->belongsTo('App\Wordbase');
    }
}
