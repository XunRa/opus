<?php

namespace App\Http\Controllers;

use App\Contracts\Word\Calculation;
use App\Contracts\Wordbase\Mapping;
use App\Contracts\Wordbase\Parser;
use App\Exceptions\Wordbase\Upload\FailedToCreateWordbaseException;
use App\Exceptions\Wordbase\Upload\InvalidFileException;
use App\Exceptions\Wordbase\Upload\NoRowsFoundInFileException;
use App\Http\Requests\Wordbase\AnagramRequest;
use App\Http\Requests\Wordbase\StoreRequest;
use App\Word;
use App\Wordbase;

class WordbaseController extends Controller
{
    public function anagrams(Wordbase $wordbase, AnagramRequest $request, Calculation $calculator, Parser $fileParser)
    {
        // Sanitize the word
        $word = $fileParser->sanitizeSingle($request->get('word'));

        // Find a matching calculated value word collection
        $words = Word::query()
            ->where('wordbase_id', '=', $wordbase->id)
            ->where(
                'calculated_value',
                '=',
                $calculator->generate($wordbase->char_mapping, $word)
            )
            ->pluck('word')
            ->toArray();

        return ['words' => $words];
    }

    public function store(
        StoreRequest $request,
        Parser $fileParser,
        Mapping $mapper,
        Calculation $calculator
    ) {
        // Retrieve the file contents and check there is something
        $contents = file_get_contents($request->file('file')->getRealPath());
        if ($contents === false || empty($contents)) {
            throw new InvalidFileException();
        }

        // Parse all the words
        $words = $fileParser->parse($contents);
        if (empty($words)) {
            throw new NoRowsFoundInFileException();
        }

        // Create a character mapping
        $mapping = $mapper->generate($words);
        if (empty($mapping)) {
            throw new NoRowsFoundInFileException();
        }

        // Save wordbase to db
        /** @var Wordbase $wordbase */
        $wordbase = $request->user()->wordbases()->create(['char_mapping' => $mapping]);

        if (empty($wordbase)) {
            throw new FailedToCreateWordbaseException();
        }

        // Create a bulk insertion array
        $insertionArray = [];
        foreach ($words as $word) {
            $insertionArray[] = [
                'word'             => $word,
                'calculated_value' => $calculator->generate($mapping, $word),
                'wordbase_id'      => $wordbase->id
            ];
        }

        // Store all the words in bulk
        Wordbase::createBulk($insertionArray);

        return [
            'wordbase_id' => $wordbase->id
        ];
    }
}
