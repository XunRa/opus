<?php

namespace App\Services\Word\Calculation;

use App\Contracts\Word\Calculation;

class Prime implements Calculation
{

    /**
     * @inheritDoc
     */
    public function generate($charMapping = [], string $word = '')
    {
        if (empty($charMapping) || empty($word)) {
            return 0;
        }
        $wordChars = mb_str_split($word);

        // Start from 1 since 0*100=0
        $sum       = 1;
        foreach ($wordChars as $char) {
            $sum *= $charMapping[$char];
        }

        return $sum;
    }
}
