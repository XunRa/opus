<?php

namespace App\Services\Wordbase\Parser;

use App\Contracts\Wordbase\Parser;

class NewLineParser implements Parser
{

    /**
     * @inheritDoc
     */
    public function parse(string $contents)
    {
        return $this->sanitize(explode("\n", $contents));
    }

    /**
     * @inheritDoc
     */
    public function sanitizeSingle(string $word)
    {
        return mb_strtolower(trim($word));
    }

    /**
     * Trims whitespace and removes empty elements
     *
     * @param array $parsed
     *
     * @return array
     */
    private function sanitize(array $parsed)
    {
        $result = [];
        foreach ($parsed as $item) {
            $sanitized = $this->sanitizeSingle($item);
            // Make sure something is left to use
            if (empty($sanitized)) {
                continue;
            }
            $result[] = $sanitized;
        }

        return $result;
    }
}
