<?php

namespace App\Services\Wordbase\Mapping;

use App\Contracts\Wordbase\Mapping;
use App\Exceptions\Wordbase\Mapping\NoPrimeFoundException;

class Prime implements Mapping
{

    /**
     * @inheritDoc
     * @throws NoPrimeFoundException
     */
    public function generate(iterable $words)
    {
        // Gather all the unique characters from words
        $chars       = [];
        foreach ($words as $word) {
            if (empty($word)) {
                continue;
            }
            $wordChars = mb_str_split($word);
            $chars     = array_unique(array_merge($chars, $wordChars));
        }

        // Assign a prime number to each character
        $charMapping = [];
        $lastPrime = 0;
        foreach ($chars as $char) {
            $nextPrime          = $this->getNextPrime($lastPrime);
            if ($nextPrime <= 1) {
                throw new NoPrimeFoundException();
            }
            $charMapping[$char] = $nextPrime;
            $lastPrime          = $nextPrime;
        }

        return $charMapping;
    }

    /**
     * Find the next prime number after the last one
     *
     * @param int $last
     *
     * @return int
     */
    private function getNextPrime($last = 0)
    {
        // Use a for loop instead of while to make sure we don't get stuck in a loop
        for ($i = $last + 1; $i <= ($last + 1) * 1000; $i++) {
            if ($this->isPrime($i)) {
                return $i;
            }
        }

        return -1;
    }

    /**
     * @author https://stackoverflow.com/questions/16763322/a-formula-to-find-prime-numbers-in-a-loop
     *
     * @param int $num
     *
     * @return bool
     */
    private function isPrime(int $num)
    {
        //1 is not prime. See: http://en.wikipedia.org/wiki/Prime_number#Primality_of_one
        if ($num == 1) {
            return false;
        }

        //2 is prime (the only even number that is prime)
        if ($num == 2) {
            return true;
        }

        /**
         * if the number is divisible by two, then it's not prime and it's no longer
         * needed to check other even numbers
         */
        if ($num % 2 == 0) {
            return false;
        }

        /**
         * Checks the odd numbers. If any of them is a factor, then it returns false.
         * The sqrt can be an aproximation, hence just for the sake of
         * security, one rounds it to the next highest integer value.
         */
        $ceil = ceil(sqrt($num));
        for ($i = 3; $i <= $ceil; $i = $i + 2) {
            if ($num % $i == 0) {
                return false;
            }
        }

        return true;
    }
}
