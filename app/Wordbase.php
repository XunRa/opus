<?php

namespace App;

use App\Exceptions\Wordbase\Upload\FailedToInsertWordsException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Wordbase
 *
 * @package App
 * @property string[] char_mapping
 * @property int      id
 */
class Wordbase extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'char_mapping'
    ];

    /**
     * Get the words for this wordbase
     */
    public function words()
    {
        return $this->hasMany('App\Word');
    }

    /**
     * Get the user who uploaded it
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
     * Decode the string since it's encoded in the database
     *
     * @param string $value
     *
     * @return string[]
     */
    public function getCharMappingAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * Encode the array for the database
     *
     * @param string[] $value
     */
    public function setCharMappingAttribute($value)
    {
        $this->attributes['char_mapping'] = json_encode($value);
    }

    public static function createBulk(array $elements)
    {
        // For speed make the insertion inside a transaction
        try {
            DB::beginTransaction();
            // Create chunks of the insertion data for faster insertion
            $chunks = array_chunk($elements, 500, true);
            foreach ($chunks as $chunk) {
                // try to insert it
                if (!DB::table('words')->insert($chunk)) {
                    throw new FailedToInsertWordsException();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
