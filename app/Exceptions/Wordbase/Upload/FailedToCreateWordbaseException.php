<?php

namespace App\Exceptions\Wordbase\Upload;

use Exception;

class FailedToCreateWordbaseException extends Exception
{
    //
}
