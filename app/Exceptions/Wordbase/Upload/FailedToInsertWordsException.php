<?php

namespace App\Exceptions\Wordbase\Upload;

use Exception;

class FailedToInsertWordsException extends Exception
{
    //
}
