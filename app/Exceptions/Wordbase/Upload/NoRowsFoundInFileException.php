<?php

namespace App\Exceptions\Wordbase\Upload;

use Exception;

class NoRowsFoundInFileException extends Exception
{
    //
}
