<?php

namespace Tests\Word\Calculation;

use App\Services\Word\Calculation\Prime;
use Tests\TestCase;

class PrimeTest extends TestCase
{

    public static $TEST_WORD = 'word';
    public static $TEST_MAPPING = ['w' => 1, 'o' => 2, 'r' => 3, 'd' => 4];
    public static $TEST_VALUE = 24; // 1*1*2*3*4

    public function testGenerate()
    {
        $prime = new Prime();
        $this->assertEquals(0, $prime->generate([], PrimeTest::$TEST_WORD));
        $this->assertEquals(0, $prime->generate(PrimeTest::$TEST_MAPPING, ''));
        $this->assertEquals(PrimeTest::$TEST_VALUE, $prime->generate(PrimeTest::$TEST_MAPPING, PrimeTest::$TEST_WORD));
    }
}
