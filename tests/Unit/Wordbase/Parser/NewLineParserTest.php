<?php

namespace Tests\Wordbase\Parser;

use App\Services\Wordbase\Parser\NewLineParser;
use Tests\TestCase;

class NewLineParserTest extends TestCase
{

    private $testOkContents = "test\nwords\nhere";
    private $testOkResults = ['test', 'words', 'here'];
    private $testSanitizeContents = "TeSt  \n wOrds\r\t\nhere\n";
    private $testSanitizeResults = ['test', 'words', 'here'];
    private $testSanitizeSingle = "TesT  \r";
    private $testSanitizeSingleResult = "test";

    public function testParse()
    {
        $parser = new NewLineParser();
        $this->assertEquals([], $parser->parse(''));
        $this->assertEquals($this->testOkResults, $parser->parse($this->testOkContents));
        $this->assertEquals($this->testSanitizeResults, $parser->parse($this->testSanitizeContents));
    }

    public function testSanitizeSingle()
    {
        $parser = new NewLineParser();
        $this->assertEquals($this->testSanitizeSingleResult, $parser->sanitizeSingle($this->testSanitizeSingle));
    }
}
