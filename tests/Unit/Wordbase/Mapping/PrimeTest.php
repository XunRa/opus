<?php

namespace Tests\Wordbase\Mapping;

use App\Services\Wordbase\Mapping\Prime;
use Tests\TestCase;

class PrimeTest extends TestCase
{

    public static $TEST_WORDS            = ['word', 'is', 'here'];
    public static $TEST_EXPECTED_MAPPING = [
        'w' => 2,
        'o' => 3,
        'r' => 5,
        'd' => 7,
        'i' => 11,
        's' => 13,
        'h' => 17,
        'e' => 19
    ];

    public function testGenerate()
    {
        $prime = new Prime();
        $this->assertEquals([], $prime->generate([]));
        $this->assertEquals(PrimeTest::$TEST_EXPECTED_MAPPING,
            $prime->generate(PrimeTest::$TEST_WORDS));
    }
}
