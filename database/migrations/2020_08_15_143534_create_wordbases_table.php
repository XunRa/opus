<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWordbasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wordbases', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->comment('Who imported it');
            $table->text('char_mapping')
                ->comment('Character map for the wordbase of chars to prime numbers, stored in json format and calculated at upload');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wordbases');
    }
}
