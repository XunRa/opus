# Opus developer test

## How to run things

### For testing of the assignment

* Copy/Rename .env.example to .env
* Run 
``docker-compose -f docker-compose.prod.yml up``
* Open [localhost](http://localhost)
* Email ``opus-test@opus.test`` and Password ``password``

### For development

* Copy/Rename .env.example to .env
* Install php and js packages
``composer install && npm install``
* Run js watcher
``npm run watch``
* Run docker compose for mysql and php
``docker-compose up``
* When changing dockerfile run
``docker-compose up --build``
to rebuild and start

### To run phpunit tests

``vendor/bin/phpunit --testsuite Unit``

### To run phpmd

``vendor/bin/phpmd app text ./phpmd-ruleset.xml``

### To run PHP Code sniffer

``vendor/bin/phpcs ./app``

### To run PHP Copy/Paste Detector

``php phpcpd.phar ./app``
